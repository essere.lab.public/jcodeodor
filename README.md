# JCodeOdor
JCodeOdor is a tool for code smells detection. It bases its detection upon the detection of 43 metrics, and provides original features, like the filtering of false positives and the sorting of results basing on their harmfulness.


## Description
JCodeOdor is distributed as a single zip file, containing the program and the precomputed metric values for 74 systems. To run JCodeOdor double-click JCodeOdor.jar, or type java -jar JCodeOdor.jar from the command line. The GUI will show up.
From the GUI, it is possible to select one of the 74 systems, and run the detection. The list of the detected instances for each code smell type is shown in the respective tab. Results are sorted by Harmfulness value.

Please notice that the largest systems among the 74 available require more than 2Gb of RAM to be analyzed. They are:

- poi-3.6
- argouml-0.34
- jruby-1.5.2
- weka-3.7.5

The smaller systems, instead, have small RAM and CPU requirements, and can be used to quickly show the tool’s capabilities:

- fitjava-1.1
- jasml-0.10
- jFin_DateMath-R1.0.1
- nekohtml-1.9.14

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
See the [JCodeOdor CLI documentation.](http://www.essere.disco.unimib.it/jcodeodor-cli-documentation/)

## Authors and acknowledgment
### Publications
Arcelli Fontana, Francesca, Vincenzo Ferme, Marco Zanoni, and Riccardo Roveda. 2015. “Towards a Prioritization of Code Debt: A Code Smell Intensity Index.” In Proceedings of the Seventh International Workshop on Managing Technical Debt (Mtd 2015), 16–24. Bremen, Germany: IEEE. doi:10.1109/MTD.2015.7332620.

Arcelli Fontana, Francesca, Vincenzo Ferme, and Marco Zanoni. 2015. “Filtering Code Smells Detection Results.” In Proceedings of the 37th International Conference on Software Engineering (ICSE 2015). Florence, Italy: IEEE.

Arcelli Fontana, Francesca, Vincenzo Ferme, and Marco Zanoni. 2015. “Towards Assessing Software Architecture Quality by Exploiting Code Smell Relations.” In Proceedings of the Second International Workshop on Software Architecture and Metrics (SAM 2015). Florence, Italy: IEEE.

### License
For citing:

Arcelli Fontana, Francesca, Vincenzo Ferme, Marco Zanoni, and Riccardo Roveda. 2015. “Towards a Prioritization of Code Debt: A Code Smell Intensity Index.” In Proceedings of the Seventh International Workshop on Managing Technical Debt (Mtd 2015), 16–24. Bremen, Germany: IEEE. doi: 10.1109/MTD.2015.7332620.

## Project status
Last modified: 2022
